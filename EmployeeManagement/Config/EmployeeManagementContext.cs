﻿using Microsoft.EntityFrameworkCore;
using EmployeeManagement.Employees.domain;
using EmployeeManagement.Teams.domain;

namespace EmployeeManagement.Config
{
    public class EmployeeManagementContext : SQLServerDbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Team> Teams { get; set; }

        public EmployeeManagementContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

           modelBuilder
                .Owned<Domainprimitives.Address>();

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).ValueGeneratedOnAdd();
                entity.OwnsOne(e => e.Address)
                     .WithOwner();
                entity.HasOne(e => e.Superior);
            });

            modelBuilder.Entity<Team>(entity =>
            {
                entity.HasKey(o => o.Id);
                entity.Property(o => o.Id).ValueGeneratedOnAdd();
                entity.HasMany(o => o.Employees);
            });
        }
    }

}
